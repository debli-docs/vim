execute pathogen#infect()
syntax on
filetype indent off
filetype plugin off

if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

if has("autocmd")
	"  filetype plugin indent on
endif

set nocompatible
if exists("&autoread")
	set autoread
endif
set cindent
set nu
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hidden             " Hide buffers when they are abandoned
set backspace=indent,eol,start
set mouse=a		" Enable mouse usage (all modes)
au BufRead *.html set ai sw=2
au BufRead *.twig set ai filetype=html sw=2
au BufRead *.tpl set ai sw=2
au BufRead *.sh set ai sw=2
au BufRead *.js set ai sw=2
au BufRead *.php set ts=4
au BufRead *.php filetype indent off
au BufRead *.php filetype plugin off
au BufRead *.php set cindent
set cino=(0
au BufRead *.php set ai sw=4
au BufRead *.tex set ai sw=2

set ls=2
set statusline=[%n]\ %f%m%r%h\ \|\ \ pwd:\ %{getcwd()}\ \ \|%=\|\ %l,%c\ %p%%\ \|\ ascii=%b,hex=%b%{((&fenc==\"\")?\"\":\"\ \|\ \".&fenc)}\ \|\ %{$USER}\ @\ %{hostname()}\

map <F5> :call Compile()<CR>
func! Compile()
	exec "w"
	if &filetype == 'c'
		exec "!g++ % -o %<"
		exec "! ./%<"
	elseif &filetype == 'cpp'
		exec "!g++ % -o %<"
		exec "! ./%<"
	elseif &filetype == 'java' 
		exec "!javac %" 
		exec "!java %<"
	elseif &filetype == 'sh'
		:!./%
	elseif &filetype == 'py'
		exec "!python %"
		exec "!python %<"
	elseif  &filetype == 'tex2'
		exec "!xelatex % && killall  xpdf.real ;  xpdf %<.pdf &"
	elseif  &filetype == 'tex'
		exec "!xelatex ./br-reportsys-detail-design.tex && killall  xpdf.real ;  xpdf ./br-reportsys-detail-design.pdf &"
	endif
endfunc

set cryptmethod=blowfish

let g:NERDTree_title="[NERDTree]"  
let g:winManagerWindowLayout="NERDTree|TagList"  

function! NERDTree_Start()  
	exec 'NERDTree'  
endfunction  

function! NERDTree_IsValid()  
	return 1  
endfunction  

nmap wm :WMToggle<CR>
nmap ft :filetype indent on<CR>:filetype plugin on<CR>:e %<CR>
nmap ff :filetype indent off<CR>:filetype plugin off<CR>:set cindent<CR>:e %<CR>

inoremap <C-i> <ESC>:call PhpDocSingle()<CR>i
nnoremap <C-i> :call PhpDocSingle()<CR>
vnoremap <C-i> :call PhpDocRange()<CR>

if exists("&foldenable")
set fen
endif

if exists("&foldlevel")
set fdl=0
endif
